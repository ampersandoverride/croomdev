package com.sample.croom;

/**
 * Created by Carlo on 02/12/2017.
 */

public class RentingTransaction {

    public String transactionNo, renterId, timeConsumed, startDate, endDate, startTime, endTime;
    public int fee;

    public RentingTransaction(String transactionNo, String renterId, String timeConsumed, String startDate, String endDate, String startTime, String endTime, int fee) {

        this.transactionNo = transactionNo;
        this.renterId = renterId;
        this.timeConsumed = timeConsumed;
        this.startDate = startDate;
        this.endDate = endDate;
        this.startTime = startTime;
        this.endTime = endTime;
        this.fee = fee;

    }

}
