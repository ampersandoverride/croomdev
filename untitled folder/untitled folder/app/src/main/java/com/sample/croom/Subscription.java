package com.sample.croom;

/**
 * Created by Carlo on 02/12/2017.
 */

public class Subscription {

    public String subscriptionNo, plan, startDateTime, endDateTime;
    public double monthlyFee;

    public Subscription(String subscriptionNo, String plan, String startDateTime, String endDateTime, double monthlyFee) {

        this.subscriptionNo = subscriptionNo;
        this.plan = plan;
        this.startDateTime = startDateTime;
        this.endDateTime = endDateTime;
        this.monthlyFee = monthlyFee;

    }

}
