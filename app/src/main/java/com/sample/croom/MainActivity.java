package com.sample.croom;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.util.Log;

public class MainActivity extends AppCompatActivity {

    private Database db = new Database(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button next = (Button) findViewById(R.id.next);
        next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Renter renter = new Renter();

                renter.email =((EditText)findViewById(R.id.et_Email)).getText().toString();
                renter.firstName =((EditText)findViewById(R.id.et_firstname)).getText().toString();
                renter.lastName =((EditText)findViewById(R.id.et_lastname)).getText().toString();
                renter.middleName =((EditText)findViewById(R.id.et_middlename)).getText().toString();
                renter.bday =((EditText)findViewById(R.id.editText2)).getText().toString();

                db.addRenter(renter);
                Log.d("HAHAHA", db.getRenter("0").renterId + " ");

                startActivity(new Intent(view.getContext(), MapsActivity.class));

            }
        });
    }

}
