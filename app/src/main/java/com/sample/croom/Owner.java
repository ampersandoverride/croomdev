package com.sample.croom;

/**
 * Created by Carlo on 02/12/2017.
 */

public class Owner {

    public int ownerId;
    public String  lastName, firstName, middleName, age, gender, bday, civilStatus, mobileNumber, telephoneNumber, email, address, latitude, longitude;

    public Owner(int ownerId, String lastName, String firstName, String middleName, String age, String gender, String bday, String civilStatus, String mobileNumber, String telephoneNumber, String email, String address, String latitude, String longitude) {
        this.ownerId = ownerId;
        this.age = age;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.gender = gender;
        this.bday = bday;
        this.civilStatus = civilStatus;
        this.mobileNumber = mobileNumber;
        this.telephoneNumber = telephoneNumber;
        this.email = email;
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
    }

}
