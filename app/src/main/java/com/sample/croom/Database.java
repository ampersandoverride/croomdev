package com.sample.croom;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Carlo on 02/12/2017.
 */

public class Database extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "croomdb";

    public Database(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);

    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {

        String createOwnerTable = "create table owner (ownerId integer primary key autoincrement, age text, lastName text, firstName text, middleName text, gender text, bday text, civilStatus text, mobileNumber text, telephoneNumber text, email text, address text, latitude text, longitude text)";
        String createRenterTable = "create table renter (renterId integer primary key autoincrement, age text, lastName text, firstName text, middleName text, gender text, bday text, civilStatus text, mobileNumber text, telephoneNumber text, email text, address text)";
        String createRentingTransactionTable = "create table rent_transaction (transactionNo integer primary key autoincrement, renterId text, timeConsumed text, startDate text, endDate text, startTime text, endTime text)";
        String createSubscriptionTable = "create table subscription (subscriptionNo integer primary key autoincrement, plan text, startDateTime text, endDateTime text)";

        sqLiteDatabase.execSQL(createOwnerTable);
        sqLiteDatabase.execSQL(createRenterTable);
        sqLiteDatabase.execSQL(createRentingTransactionTable);
        sqLiteDatabase.execSQL(createSubscriptionTable);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

        sqLiteDatabase.execSQL("drop table if exists owner");
        sqLiteDatabase.execSQL("drop table if exists renter");
        sqLiteDatabase.execSQL("drop table if exists rent_transaction");
        sqLiteDatabase.execSQL("drop table if exists subscription");

        onCreate(sqLiteDatabase);

    }

    public void addRenter(Renter renter) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("lastName",renter.lastName);
        values.put("firstName", renter.firstName);
        values.put("middleName", renter.middleName);
        values.put("bday", renter.bday);
        values.put("email", renter.email);

        sqLiteDatabase.insert("renter", null, values);
        sqLiteDatabase.close();


    }

    public void addOwner(Owner owner) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("ownerId", owner.ownerId);
        values.put("age", owner.age);
        values.put("lastName", owner.lastName);
        values.put("firstName", owner.firstName);
        values.put("middleName", owner.middleName);
        values.put("gender", owner.gender);
        values.put("bday", owner.bday);
        values.put("civilStatus", owner.civilStatus);
        values.put("mobileNumber", owner.mobileNumber);
        values.put("telephoneNumber", owner.telephoneNumber);
        values.put("civilStatus", owner.civilStatus);
        values.put("email", owner.email);
        values.put("address", owner.address);
        values.put("latitude", owner.latitude);
        values.put("longitude", owner.longitude);

        sqLiteDatabase.insert("owner", null, values);
        sqLiteDatabase.close();


    }

    public void addRentingTransaction(RentingTransaction rentingTransaction) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("transactionNo", rentingTransaction. transactionNo);
        values.put("renterID", rentingTransaction.renterId);
        values.put("timeConsumed", rentingTransaction.timeConsumed);
        values.put("startDate", rentingTransaction.startDate);
        values.put("endDate", rentingTransaction.endDate);
        values.put("startTime", rentingTransaction.startTime);
        values.put("endTime", rentingTransaction.endTime);

        sqLiteDatabase.insert("RentingTransaction", null, values);
        sqLiteDatabase.close();
    }

    public void addSubscription(Subscription subscription) {

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("subscriptionNo", subscription.subscriptionNo);
        values.put("plan", subscription.plan);
        values.put("startDateTime", subscription.startDateTime);
        values.put("endDateTime", subscription.endDateTime);

        sqLiteDatabase.insert("subscription", null, values);
        sqLiteDatabase.close();

    }

    public Owner getOwner(String ownerId) {

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query("owner", new String[] {"ownerId", "lastName", "firstName", "middleName", "age", "gender", "bday", "civilStatus", "mobileNumber", "telephoneNumber", "email", "address"}, "ownerId = " + ownerId, new String[] {"ownerId"}, null, null, null, null);

        if(cursor != null) {

            cursor.moveToFirst();

        }

        Owner owner = new Owner(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12), cursor.getString(13));

        return owner;

    }

    public Renter getRenter(String renterId) {

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query("renter", new String[] {"renterId", "lastName", "firstName", "middleName", "bday", "email"}, "renterId = ?", new String[] { renterId }, null, null, null, null);

        if(cursor != null) {

            cursor.moveToFirst();

        }

        Renter renter = new Renter(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5));

        return renter;

    }

    public RentingTransaction getRentingTransaction(String transactionNo) {

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query("rent_transaction", new String[] {"transactionNo", "renterId", "timeConsumed", "startDate", "endDate", "startTime", "endTime", "fee"}, "transactionNo = " + transactionNo, new String[] {"transactionNo"}, null, null, null, null );

        if(cursor != null) {

            cursor.moveToFirst();

        }

        RentingTransaction rentingTransaction = new RentingTransaction(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), Integer.parseInt(cursor.getString(7)));

        return rentingTransaction;
    }

    public Subscription getsubscription(String subscriptionNo) {

        SQLiteDatabase sqLiteDatabase = this.getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query("subscription", new String[] {"subscriptionNo", "plan", "startDateTime", "endDateTime", "monthlyFee"}, "subscriptionNo = " + subscriptionNo, new String[] {"subscriptionNo"}, null, null, null, null );

        if(cursor != null) {

            cursor.moveToFirst();

        }

        Subscription subscription = new Subscription(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), Double.parseDouble(cursor.getString(4)));

        return subscription;

    }

    public List<Owner> getAllOwner() {
        List<Owner> ownerList = new ArrayList<Owner>();

        String selectQuery = "SELECT * FROM owner ";

        SQLiteDatabase sqLiteDatabase = this.getWritableDatabase();
        Cursor cursor = sqLiteDatabase.rawQuery(selectQuery, null);

        if(cursor.moveToFirst()) {
            do {
                Owner owner = new Owner(Integer.parseInt(cursor.getString(0)), cursor.getString(1), cursor.getString(2), cursor.getString(3), cursor.getString(4), cursor.getString(5), cursor.getString(6), cursor.getString(7), cursor.getString(8), cursor.getString(9), cursor.getString(10), cursor.getString(11), cursor.getString(12), cursor.getString(13));
                ownerList.add(owner);

            }while (cursor.moveToNext());
        }

        return ownerList;
    }
}
