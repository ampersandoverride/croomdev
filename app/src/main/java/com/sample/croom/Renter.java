package com.sample.croom;

/**
 * Created by Carlo on 02/12/2017.
 */

public class Renter {

    public int renterId;
    public String lastName, firstName, middleName, bday, email;

    public Renter() {}

    public Renter(int renterId, String lastName, String firstName, String middleName, String bday, String email) {
        this.renterId = renterId;
        this.lastName = lastName;
        this.firstName = firstName;
        this.middleName = middleName;
        this.bday = bday;
        this.email = email;
    }
}
